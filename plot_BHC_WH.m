%% bar plot per IDP per subj


data = readtable('IDPs_compared_BHC+WH.xlsx');

% without GDC IDPs
no_gdc_data = [data.W3T_2019_102_017_noGDC, data.W3T_2019_102_020_noGDC,...
               data.W3T_2019_102_033_noGDC, data.W3T_2019_102_042_noGDC,...
               data.W3T_2019_102_065_noGDC, data.W3T_2019_102_100_noGDC,...
               data.WH_015_withoutGDC, data.WH_143_withoutGDC, data.WH_700_withoutGDC, data.WH_800_withoutGDC]';
% with GDC IDPs
gdc_data    = [data.W3T_2019_102_017, data.W3T_2019_102_020,...
               data.W3T_2019_102_033, data.W3T_2019_102_042,...
               data.W3T_2019_102_065, data.W3T_2019_102_100,...
               data.WH_015_withGDC, data.WH_143_withGDC, data.WH_700_withGDC, data.WH_800_withGDC]';

featurenames = data.IDP_name;

if ~exist('plots_dir','dir')
    mkdir('plots_dir')
end

cd('plots_dir')

for ii = 1:length(featurenames)    
    bar([gdc_data(:,ii),no_gdc_data(:,ii)])
    xlabel('subjects')
    legend({'with GDC','without-GDC'})
    xticklabels({'subj-01-BHC','subj-02-BHC','subj-03-BHC','subj-04-BHC','subj-05-BHC','subj-06-BHC','subj-07-WH','subj-08-WH','subj-09-WH','subj-10-WH'})
    box('off')
    grid('on')
    title(strrep(featurenames{ii,1},'_','-'))
    saveas(gcf,sprintf('%s.fig',strrep(featurenames{ii,1},'_','-')));
    print(featurenames{ii,1},'-dpng','-r300')
    close(gcf)
end

%% IDP difference (absolute)

if ~exist('plots_dir','dir')
    mkdir('plots_dir')
end

cd('plots_dir')
data1 = data;
uq_features  = unique(data1.script);

for uq = 1:length(uq_features)
    scr_name    = uq_features{uq,1}; 
    data        = data1(ismember(data1.script,scr_name),:);
    % without GDC IDPs
    no_gdc_data = [data.W3T_2019_102_017_noGDC, data.W3T_2019_102_020_noGDC,...
                   data.W3T_2019_102_033_noGDC, data.W3T_2019_102_042_noGDC,...
                   data.W3T_2019_102_065_noGDC, data.W3T_2019_102_100_noGDC,...
                   data.WH_015_withoutGDC, data.WH_143_withoutGDC, data.WH_700_withoutGDC, data.WH_800_withoutGDC];
    % with GDC IDPs
    gdc_data    = [data.W3T_2019_102_017, data.W3T_2019_102_020,...
                   data.W3T_2019_102_033, data.W3T_2019_102_042,...
                   data.W3T_2019_102_065, data.W3T_2019_102_100,...
                   data.WH_015_withGDC, data.WH_143_withGDC, data.WH_700_withGDC, data.WH_800_withGDC];
    for subj = 1:10
        subplot(5,2,subj)
        diff_val    = abs(gdc_data(:,subj)-no_gdc_data(:,subj));
        vals_idp    = [data.plot_var,num2cell(diff_val)];
        [~, idx]    = sort([vals_idp{:,2}], 'descend');
        vals_sorted = vals_idp(idx,:);
        bar(cell2mat(vals_sorted(:,2)));
        title(sprintf('subject-%d',subj));
        xticklabels(strrep(vals_sorted(:,1),'_','-'))
        a = get(gca,'XTickLabel');  
        set(gca,'XTickLabel',a,'fontsize',2)
        box('off')
        grid('on')
    
    end
    saveas(gcf,['00_',scr_name,'.fig'])
    print(['00_',scr_name],'-dpng','-r300')
    close(gcf)
end
