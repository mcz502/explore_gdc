%% Get the data and harmonize
data       = readtable('/Users/psyc1586_admin/r_dpuk/explore_GDC/plots_dir/IDPs_compared_BHC+WH.xlsx');
data_gdc   = [data.WH_015_withGDC, data.WH_143_withGDC, data.WH_700_withGDC, data.WH_800_withGDC];
data_nogdc = [data.WH_015_withoutGDC, data.WH_143_withoutGDC, data.WH_700_withoutGDC, data.WH_800_withoutGDC];

scan_id    = [1,1,2,2];

data_harmonized_gdc   = combat(data_gdc,   scan_id, [], 1);
data_harmonized_nogdc = combat(data_nogdc, scan_id, [], 1);

data_table = [data.IDP_name, data.plot_var, data.script, array2table(data_harmonized_gdc),array2table(data_harmonized_nogdc)];
data_table.Properties.VariableNames = {'IDP_name','plot_var','script','WH_017_gdc_harm', 'WH_143_gdc_harm', 'WH_700_gdc_harm','WH_800_gdc_harm',...
                                       'WH_017_nogdc_harm', 'WH_143_nogdc_harm', 'WH_700_nogdc_harm','WH_800_nogdc_harm'};

%% plot the differences
data1        = data_table;
uq_features  = unique(data1.script);

for uq = 1:length(uq_features)
    scr_name    = uq_features{uq,1}; 
    data        = data1(ismember(data1.script,scr_name),:);
    % without GDC IDPs
    gdc_data      = [data.WH_017_gdc_harm, data.WH_143_gdc_harm,...
                   data.WH_700_gdc_harm, data.WH_800_gdc_harm];
    % with GDC IDPs
    no_gdc_data    = [data.WH_017_nogdc_harm, data.WH_143_nogdc_harm,...
                   data.WH_700_nogdc_harm, data.WH_800_nogdc_harm];

    for subj = 1:4
        subplot(4,1,subj)
        diff_val    = abs(gdc_data(:,subj)-no_gdc_data(:,subj));
        vals_idp    = [data.plot_var,num2cell(diff_val)];
        [~, idx]    = sort([vals_idp{:,2}], 'descend');
        vals_sorted = vals_idp(idx,:);
        bar(cell2mat(vals_sorted(:,2)));
        title(sprintf('subject-%d',subj));
        xticklabels(strrep(vals_sorted(:,1),'_','-'))
        a = get(gca,'XTickLabel');  
        set(gca,'XTickLabel',a,'fontsize',5)
        box('off')
        grid('on')
    
    end
    saveas(gcf,['00_',scr_name,'.fig'])
    print(['00_',scr_name],'-dpng','-r300')
    close(gcf)
end





