# Explore GDC

These scripts are written to explore change of IDPs with and without the Gradient distortion correction is applied to the T1w processing in UK biobank pipeline. 

Input: 6 subjects from BHC data; 4 subjects from 2 different scanner from Whitehall data

Procedure:

1) Process each subject in with GDC pipeline (.grad is needed)
2) Process each subject in without GDC pipeline
3) Calculate IDPs for both case 1 and 2
4) Plot these IDPs; plot the difference in IDPs with and without GDC
5) Harmonise the IDPs separately for with and without GDC; plot these IDPs; also plot the difference in harmonised IDPs with and without GDC
